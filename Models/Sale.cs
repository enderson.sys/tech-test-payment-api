﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using PaymentAPI.Models.Enums;

namespace PaymentAPI.Models
{
    public class Sale
    {
        public int Id { get; set; }
        public DateTime SaleDate { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public SaleStatus Status;

        public Seller Seller { get; set; }

        public List<Product> Products = new();


        public Sale(int id, Seller seller)
        {
            Id = id;
            SaleDate = DateTime.Now;
            Seller = seller;
            Status = SaleStatus.PendingPayment;
        }

        public void AddProduct(Product product)
        {
            Products.Add(product);
        }

        public string Serialized(Formatting formating = Formatting.Indented)
        {
            return JsonConvert.SerializeObject(this, formating);
        }
    }
}
