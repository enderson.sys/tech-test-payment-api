﻿using System.Runtime.Serialization;

namespace PaymentAPI.Models.Enums
{
    public enum SaleStatus
    {
        [EnumMember(Value ="Aguardando pagamento")]
        PendingPayment,
        [EnumMember(Value = "Pagamento aprovado")]
        ApprovedPayment,
        [EnumMember(Value = "Enviado para transportadora")]
        SentToCarrier,
        [EnumMember(Value = "Entregue")]
        Delivered,
        [EnumMember(Value = "Cancelada")]
        Canceled
    }
}
