﻿using PaymentAPI.Models;
using PaymentAPI.Models.Enums;

namespace PaymentAPI.Services
{
    public class SaleService
    {
        private List<Sale> SalesList = new();

        public List<Sale> GetSalesList()
        {
            return SalesList;
        }

        public Sale CreateSale(
            int saleId,
            int userId,
            string userCpf,
            string userName,
            string userEmail,
            string userPhoneNumber,
            List<Product> products
        )
        {
            Sale sale = new Sale(saleId, new Seller(userId, userCpf, userName, userEmail, userPhoneNumber));

            AddProductsToSale(sale, products);

            AddSaleToSaleList(sale);

            return sale;
        }

        private void AddSaleToSaleList(Sale sale)
        {
            SalesList.Add(sale);
        }

        private void AddProductsToSale(Sale sale, List<Product> products)
        {
            foreach (var product in products)
            {
                sale.AddProduct(new Product(product.Name));
            }
        }

        public Sale UpdateSaleStatus(Sale sale, SaleStatus saleStatus)
        {
            if (sale.Status == SaleStatus.PendingPayment && (saleStatus == SaleStatus.ApprovedPayment || saleStatus == SaleStatus.Canceled))
            {
                sale.Status = saleStatus;
            }
            else if (sale.Status == SaleStatus.ApprovedPayment && (saleStatus == SaleStatus.SentToCarrier || saleStatus == SaleStatus.Canceled))
            {
                sale.Status = saleStatus;
            }
            else if (sale.Status == SaleStatus.SentToCarrier && saleStatus == SaleStatus.Delivered)
            {
                sale.Status = saleStatus;
            }

            return sale;
        }
    }
}
