﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using PaymentAPI.Models;
using PaymentAPI.Services;
using PaymentAPI.Models.Enums;

namespace PaymentAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SaleController : ControllerBase
    {
        private readonly SaleService _saleService;
        
        public SaleController(IOptions<SaleService> saleServiceOptions)
        {
            _saleService = saleServiceOptions.Value;
        }

        [HttpPost("RegisterSale")]
        public IActionResult CreateSale(
            int saleId, 
            int userId, 
            string userCpf, 
            string userName, 
            string userEmail, 
            string userPhoneNumber, 
            List<Product> products
        )
        {
            if (products.Count <= 0)
            {
                return BadRequest(new { message = "Uma venda deve conter ao menos um produto!" });
            }

            Sale sale = _saleService.CreateSale(saleId, userId, userCpf, userName, userEmail, userPhoneNumber, products);

            return Ok(sale.Serialized());
        }

        [HttpGet("GetSaleById")]
        public IActionResult GetSaleById(int id)
        {
            var sale = _saleService.GetSalesList().Find(x => x.Id == id);

            if (sale == null)
                return NotFound();

            return Ok(sale.Serialized());
        }

        [HttpPost("UpdateSaleStatus")]
        public IActionResult UpdateSaleStatus(int saleID, SaleStatus saleStatus)
        {
            var sale = _saleService.GetSalesList().Find(x => x.Id == saleID);

            if (sale == null)
                return NotFound();

            _saleService.UpdateSaleStatus(sale, saleStatus);

            return Ok(sale.Serialized());
        }
    }
}
